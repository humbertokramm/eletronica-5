from numpy import arccos, pi, amax
from capacitor import getCap

#Parâmetros do Projeto
Vmin = 110 #Vac (+/- 25%)
Vmax = 220 #Vac (+/- 25%)
f = 60 # Hz
n = 0.7
Pout = 150 # W 
Vcmin = 100 # V

print('')
print('*************************')
print('Parâmetros do exemplo de aula retificador')
Vmin = 99 #Vac (+/- 25%)
print('Vmin = ',Vmin, 'V')
Vmax = 135 #Vac (+/- 25%)
print('Vmax = ',Vmax, 'V')
f = 60 # Hz
print('f = ',f, 'Hz')
n = 0.7
print('n = ',n)
Pout = 70 # W 
print('Pout = ',Pout, 'W')
Vcmin = 100 # V
print('Vcmin = ',Vcmin, 'V')

T = 1/f
print('T = ',T*1e3, 'ms')

Pin = Pout/n
print('Pin = ',Pin, 'W')

Win = Pin/f
print('Win = ',Win, 'J')

Vpk = 2**(1/2)*Vmin
print('Vpk = ',Vpk, 'V')

Vpk = Vmax
print('Vpk = ',Vpk, 'V')

Vc1min = (2*Vcmin - Vpk)/3
print('Vc1min = ',Vc1min, 'V')

VpkMax = amax([2**(1/2)*Vmin,Vmax,2**(1/2)*Vmax])
print('VpkMax = ',VpkMax, 'V')

C = Pin/(f*(Vmax**2-Vcmin**2))
#C = Pin/(f*(Vmax**2-Vc1min**2))
print('C = ',C*1e6, 'uF')

C1 = C2 = 2*C
#C1 = C2 = C/2
print('C1 = ',C1*1e6, 'uF')
print('C2 = ',C2*1e6, 'uF')
C1comercial = getCap(C1)
print('C1comercial = ',C1comercial*1e6, 'uF')

tc = arccos(Vcmin/Vpk)/(2*pi*f)
#tc = arccos(Vc1min/Vpk)/(2*pi*f)
print('tc = ',tc*1e3, 'ms')

dV = Vpk - Vcmin
#dV = Vpk - Vc1min
print('dV = ',dV, 'V')

Ip = C*dV/tc
print('Ip = ',Ip, 'A')

Ic1ef = Ip*(2*tc*f-(2*tc*f)**2)**(1/2)
#Ic1ef = Ip*(tc*f-(tc*f)**2)**(1/2)
print('Ic1ef = ',Ic1ef, 'A')

I2ef = Pin/Vcmin
print('I2ef = ',I2ef, 'A')

Icef = (I2ef**2+Ic1ef**2)**(1/2)
print('Icef = ',Icef, 'A')

Idef = Ip*(tc/T)**(1/2)
print('Idef = ',Idef, 'A')

IDmed = Pin/(2*Vcmin)
print('IDmed = ',IDmed, 'A')

VDmax = VpkMax
#VDmax = 2*2**(1/2)*Vpk
print('VDmax = ',VDmax, 'V')

IDp = Ip
print('IDp = ',IDp, 'A')


print('')
print('*************************')
print('Parâmetros do exemplo de aula dobrador')
Vmin = 99 #Vac (+/- 25%)
print('Vmin = ',Vmin, 'V')
Vmax = 135 #Vac (+/- 25%)
print('Vmax = ',Vmax, 'V')
f = 60 # Hz
print('f = ',f, 'Hz')
n = 0.7
print('n = ',n)
Pout = 70 # W 
print('Pout = ',Pout, 'W')
Vcmin = 200 # V
print('Vcmin = ',Vcmin, 'V')

T = 1/f
print('T = ',T*1e3, 'ms')

Pin = Pout/n
print('Pin = ',Pin, 'W')

Win = Pin/f
print('Win = ',Win, 'J')

Vpk = 2**(1/2)*Vmin
print('Vpk = ',Vpk, 'V')

Vpk = Vmax
print('Vpk = ',Vpk, 'V')

Vc1min = (2*Vcmin - Vpk)/3
print('Vc1min = ',Vc1min, 'V')

VpkMax = amax([2**(1/2)*Vmin,Vmax,2**(1/2)*Vmax])
print('VpkMax = ',VpkMax, 'V')

#C = Pin/(f*(Vmax**2-Vcmin**2))
C = Pin/(f*(Vmax**2-Vc1min**2))
print('C = ',C*1e6, 'uF')

#C1 = C2 = 2*C
C1 = C2 = C/2
print('C1 = ',C1*1e6, 'uF')
print('C2 = ',C2*1e6, 'uF')

#tc = arccos(Vcmin/Vpk)/(2*pi*f)
tc = arccos(Vc1min/Vpk)/(2*pi*f)
print('tc = ',tc*1e3, 'ms')

#dV = Vpk - Vcmin
dV = Vpk - Vc1min
print('dV = ',dV, 'V')

Ip = C*dV/tc
print('Ip = ',Ip, 'A')

#Ic1ef = Ip*(2*tc*f-(2*tc*f)**2)**(1/2)
Ic1ef = Ip*(tc*f-(tc*f)**2)**(1/2)
print('Ic1ef = ',Ic1ef, 'A')

I2ef = Pin/Vcmin
print('I2ef = ',I2ef, 'A')

Icef = (I2ef**2+Ic1ef**2)**(1/2)
print('Icef = ',Icef, 'A')

Idef = Ip*(tc/T)**(1/2)
print('Idef = ',Idef, 'A')

IDmed = Pin/(2*Vcmin)
print('IDmed = ',IDmed, 'A')

#VDmax = VpkMax
VDmax = 2*2**(1/2)*Vpk
print('VDmax = ',VDmax, 'V')

IDp = Ip
print('IDp = ',IDp, 'A')