from nucleosInd import dataNucleo,dataFio
from printSI import printSI
from numpy import pi

#especificação

#exemplo
L = 20e-6	# H
IDC = 25	# A
IAC_PICO = 3	#A
f = 100e3 #Hz
dT = 30	#°C

#trabalho
L = 0.1e-3	# H
IDC = 2.4	# A
IAC_PICO = 2.5	#A

#trabalho GB
L = 110e-6	# H
IDC = 10	# A
IAC_PICO = 10	#A

#dados adotados pelo projetista
B = 0.3
Ku = 0.4
µ0 = 4*pi*1e-7
Tamp = 20

def dimensiona(index, show = True):
	# Carrega dasdos escolhidos
	if(show): print('Núcleo Escolhido:')
	if(show): print(list(dataNucleo)[index])
	AP,CEM,Le,Ae,As,Kj,X,Caminhos = list(dataNucleo.values())[index]
	printSI(AP,'AP','cm^4','',output = show)
	printSI(CEM,'CEM','c','m',output = show)
	printSI(Le,'Le','c','m',output = show)
	printSI(Ae,'Ae','cm²','',output = show)
	printSI(As,'As','cm²','',output = show)
	printSI(Kj,'Kj','','',output = show)
	printSI(X,'X','','',output = show)

	# 2 - Calculo do Kj
	Kj = Kj*(dT**0.54)
	Z = 1/(1-X)

	# 3 - Calculo do Ap
	APcalc = ((2*E*1e4)/(Ku*Kj*B))**Z/1e8
	printSI(APcalc,'APcalc','cm^4','',output = show)

	# 4 - Calculo do AL
	Al =  (Ae**2)*(B**2)/2/E
	printSI(Al,'Al','n','H/esp²',output = show)

	# 5 - Calculo do GAP
	µe = Al*Le/µ0/Ae
	printSI(µe,'µe','','',output = show)

	lg = Le/µe
	printSI(lg,'lg','m','m',output = show)

	gap =lg/Caminhos
	printSI(gap,'gap','m','m',output = show)

	# 6 - Calculo do Nº de espiras
	N = round((L/Al)**(1/2),0)
	printSI(N,'N','','',0,output = show)

	# 7 - Calculo do Fio
	J = Kj*(APcalc*1e8)**(-X)
	printSI(J,'J','','A/cm²',output = show)

	Acu = IDC/J/1e4
	printSI(Acu,'Acu','cm²','',output = show)

	γ = (4.35e3/f)**(1/2)/1e3
	printSI(γ,'γ','m','m',output = show)

	DiaUtil = γ*2
	printSI(DiaUtil,'DiaUtil','m','m',output = show)

	size = len(list(dataFio))
	for x in range(size):
		D,A,D_Isol,A_Isol,Ω_by_m_20C,Ω_by_m_100C= list(dataFio.values())[size-x-1]
		
		if(DiaUtil<=D):
			if(show): print('Utilizar fio',list(dataFio)[size-x-1],'AWG')
			areaFio = A
			break

	printSI(A,'A','cm²','',output = show)
	Elementos = int(Acu/A)
	printSI(Elementos,'Elementos','','',0,output = show)


	#8 - Perdas
	deltaB = N*Al*IAC_PICO*2/Ae
	printSI(deltaB,'deltaB','','T',output = show)

	Grafico = 80e-3
	printSI(Grafico,'Grafico','m','W/cm³',output = show)

	PerdasNucleo = Grafico*Ae*Le*1e6
	printSI(PerdasNucleo,'PerdasNucleo','','W',output = show)

	ComprimentoFio = CEM * N
	printSI(ComprimentoFio,'ComprimentoFio','c','m',output = show)

	ResTotal = Ω_by_m_20C * ComprimentoFio / Elementos
	printSI(ResTotal,'ResTotal','m','Ω',output = show)

	PerdasCobre = ResTotal*(IDC**2 + (IAC_PICO**(1/2))/2)
	printSI(PerdasCobre,'PerdasCobre','','W',output = show)

	PerdasTotais = PerdasNucleo + PerdasCobre
	printSI(PerdasTotais,'PerdasTotais','','W',output = show)
	
	deltaT = (1.821e5*PerdasTotais/((82+Tamp)*As*1e4))**0.818
	printSI(deltaT,'deltaT','','°C',output = show)
	
	return PerdasTotais,deltaT
	


printSI(L,'L','m','H')
printSI(IDC,'IDC','','A')
printSI(IAC_PICO,'IAC_PICO','','A')
printSI(f,'f','k','Hz')
printSI(dT,'dT','','°C')
printSI(B,'B','','T')
printSI(Ku,'Ku','','')

# 1 - Calculo da Energia
E = (L*(IDC+IAC_PICO)**2)/2
printSI(E,'E','m','J')

# - Busca por Núcleos Válidos
print('Opção\tNome\t\tApCalc\tAp\tPerdas\tdT')
print('\t\t\tcm^4\tcm^4\tW\t°C')
for x in range(len(list(dataNucleo))):
	AP,CEM,Le,Ae,As,Kj,X,Caminhos = list(dataNucleo.values())[x]
	Z = 1/(1-X)
	Kj = Kj*(dT**0.54)
	APcalc =((2*E*1e4)/(Ku*Kj*B))**Z/1e8

	if(APcalc<AP):
		PerdasTotais,deltaT = dimensiona(x,False)
	
		print(x,'\t',end="")
		print(list(dataNucleo)[x],'\t',end="")
		print(round(APcalc*1e8,3),'\t',end="")
		print(round(AP*1e8,3),'\t',end="")
		print(round(PerdasTotais,3),'\t',end="")
		print(int(deltaT),'\t',end="")
		
		print('')

# - Captura escolha do Usuário
x = -1
while x <= 0 or x > len(list(dataNucleo)) :
	print('Digite um número no catalogo de nucleos compatíveis')
	a = input('').split(" ")[0]
	x = int(a)
dimensiona(x)
