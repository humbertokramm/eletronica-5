from numpy import arccos, pi, amax,sqrt
from capacitor import getCap
from printSI import printSI
from pytexit import py2tex

#Parâmetros do Projeto
Vac1 = 110
Vac2 = 220
Vmin = Vac1*0.75 #Vac (+/- 25%)
Vmax = Vac2*1.25 #Vac (+/- 25%)
f = 60 # Hz
n = 0.7
Pout = 150 # W 
Vcmin = 100 # V

trabalho = ''

print('')
print('*************************')
print('Parâmetros do exemplo de aula retificador')

Vmin = 220*0.75 #Vac (+/- 25%)
Vmax = 220*1.25 #Vac (+/- 25%)

trabalho += printSI(Vmin,'Vmin','','V',latexEQU = 'Vmin = 220*0.75')
trabalho += printSI(Vmax,'Vmax','','V',latexEQU = 'Vmax = 220*1.25')

printSI(f,'f','','Hz')
printSI(n,'n','','')
printSI(Pout,'Pout','','W')
printSI(Vcmin,'Vcmin','','V')


Pin = Pout/n
trabalho += printSI(Pin,'Pin','','W',latexEQU = 'Pin = Pout/n')

Vpk = sqrt(2)*Vmin - 1.4
trabalho += printSI(Vpk,'Vpk','','V',latexEQU = 'Vpk = sqrt(2)*Vmin - 1.4')

C = Pin/(f*(Vpk**2-Vcmin**2))
trabalho += printSI(C,'C','µ','F',latexEQU = 'C = Pin/(f*(Vpk**2-Vcmin**2))')

C1 = C2 = 2*C
trabalho += printSI(C1,'C1','µ','F',latexEQU = 'C1 = C2 = 2*C')
printSI(C2,'C2','µ','F')

print('Passando para valores comerciais')
C1 = C2 = getCap(C1)
printSI(C1,'C1','µ','F')
printSI(C2,'C2','µ','F')

tc = arccos(Vcmin/Vpk)/(2*pi*f)
trabalho += printSI(tc,'tc','m','s',latexEQU = 'tc = arccos(Vcmin/Vpk)/(2*pi*f)')

dV = Vpk - Vcmin
trabalho += printSI(dV,'dV','','V',latexEQU = 'dV = Vpk - Vcmin')

Ip = (C1*1.2/2)*dV/tc
trabalho += printSI(Ip,'Ip','','A',latexEQU = 'Ip = (C1*1.2/2)*dV/tc')

Ic1ef = Ip*sqrt(2*tc*f-(2*tc*f)**2)
trabalho += printSI(Ic1ef,'Ic1ef','','A',latexEQU = 'Ic1ef = Ip*sqrt(2*tc*f-(2*tc*f)**2)')

I2ef = Pin/Vcmin
trabalho += printSI(I2ef,'I2ef','','A',latexEQU = 'I2ef = Pin/Vcmin')

Icef = sqrt(I2ef**2+Ic1ef**2)
trabalho += printSI(Icef,'Icef','','A',latexEQU = 'Icef = sqrt(I2ef**2+Ic1ef**2)')

Idef = Ip*sqrt(tc*f)
trabalho += printSI(Idef,'Idef','','A',latexEQU = 'Idef = Ip*sqrt(tc*f)')

IDmed = Pin/(2*Vcmin)
trabalho += printSI(IDmed,'IDmed','','A',latexEQU = 'IDmed = Pin/(2*Vcmin)')

VDmax = sqrt(2)*Vmax
trabalho += printSI(VDmax,'VDmax','','V',latexEQU = 'VDmax = sqrt(2)*Vmax')

IDp = Ip
trabalho += printSI(IDp,'IDp','','A',latexEQU = 'IDp = Ip')




print('')
print('*************************')
print('Parâmetros do exemplo de aula dobrador')
trabalho += '\nParâmetros do exemplo de aula dobrador\n'

Vmin = Vac1*0.75 #Vac (+/- 25%)
Vmax = Vac1*1.25 #Vac (+/- 25%)

trabalho += printSI(Vmin,'Vmin','','V',latexEQU = 'Vmin = 110*0.75')
trabalho += printSI(Vmax,'Vmax','','V',latexEQU = 'Vmax = 220*1.25')


Vpk = sqrt(2)*Vmin - 1.4
trabalho += printSI(Vpk,'Vpk','','V',latexEQU = 'Vpk = sqrt(2)*Vmin - 1.4')

Vc1min = (2*Vcmin - Vpk)/3
trabalho += printSI(Vc1min,'Vc1min','','V',latexEQU = 'Vc1min = (2*Vcmin - Vpk)/3')

C1 = C2 = Pin/(f*(Vpk**2-Vc1min**2))
trabalho += printSI(C1,'C1','µ','F',latexEQU = 'C1 = C2 = Pin/(f*(Vpk**2-Vc1min**2))')
printSI(C2,'C2','µ','F')

C = C1/2
trabalho += printSI(C,'C','µ','F',latexEQU = 'C = C1/2')


print('Passando para valores comerciais')
C1 = C2 = getCap(C1)
trabalho += printSI(C1,'C1','µ','F',latexEQU = 'C1 = C2')
printSI(C2,'C2','µ','F')

tc = arccos(Vc1min/Vpk)/(2*pi*f)
trabalho += printSI(tc,'tc','m','s',latexEQU = 'tc = arccos(Vc1min/Vpk)/(2*pi*f)')

Ip = (C1*1.2)*(Vpk-Vc1min)/tc;
trabalho += printSI(Ip,'Ip','','A',latexEQU = 'Ip = (C1*1.2)*(Vpk-Vc1min)/tc;')

Ic1ef = Ip*sqrt(tc*f-(tc*f)**2)
trabalho += printSI(Ic1ef,'Ic1ef','','A',latexEQU = 'Ic1ef = Ip*sqrt(tc*f-(tc*f)**2)')

I2ef = Pin/Vcmin
trabalho += printSI(I2ef,'I2ef','','A',latexEQU = 'I2ef = Pin/Vcmin')

Icef = sqrt(I2ef**2+Ic1ef**2)
trabalho += printSI(Icef,'Icef','','A',latexEQU = 'Icef = sqrt(I2ef**2+Ic1ef**2)')

Idef = Ip*sqrt(tc*f)
trabalho += printSI(Idef,'Idef','','A',latexEQU = 'Idef = Ip*sqrt(tc*f)')

IDmed = Ip*tc*f
trabalho += printSI(IDmed,'IDmed','','A',latexEQU = 'IDmed = Ip*tc*f')

IDp = Ip
trabalho += printSI(IDp,'IDp','','A',latexEQU = 'IDp = Ip')

print('')
print('*************************')
print('Agregando o novo valor de Capacitor de 220V em 110V')
trabalho += 'Agregando o novo valor de Capacitor de 220V em 110V'

Ip = (C1*1.2/2)*dV/tc
trabalho += printSI(Ip,'Ip','','A',latexEQU = 'Ip = (C1*1.2/2)*dV/tc')

Ic1ef = Ip*sqrt(2*tc*f-(2*tc*f)**2)
trabalho += printSI(Ic1ef,'Ic1ef','','A',latexEQU = 'Ic1ef = Ip*sqrt(2*tc*f-(2*tc*f)**2)')

Icef = sqrt(I2ef**2+Ic1ef**2)
trabalho += printSI(Icef,'Icef','','A',latexEQU = 'Icef = sqrt(I2ef**2+Ic1ef**2)')

Idef = Ip*sqrt(tc*f)
trabalho += printSI(Idef,'Idef','','A',latexEQU = 'Idef = Ip*sqrt(tc*f)')

IDp = Ip
trabalho += printSI(IDp,'IDp','','A',latexEQU = 'IDp = Ip')


print('')
print('*************************')

THD_table ={
	'3' :	92.5,
	'5' :	77.64,
	'7' :	59.28,
	'9' :	40.16,
	'11':	23.2,
	'13':	14.23,
	'15':	14.23,
	'17':	16.03,
	'19':	15.75,
	'21':	13.18,
	'23':	8.92,
	'25':	6.71,
	'27':	5.83,
	'29':	5.95,
	'31':	5.25,
	'33':	4.14,
	'35':	3.03,
	'37':	1.63,
	'39':	1.69,
	'41':	1.98,
	'43':	1.52,
	'45':	0.76,
	'47':	0.76,
	'49':	0,
	'51':	0.52
}

Vn = 0
for x in range(len(list(THD_table))):
	Vn += (list(THD_table.values())[x]/100)**2

THD = sqrt(Vn)*100
trabalho += printSI(THD,'THD','','%',latexEQU = 'THD = sqrt(Vn)*100')



print(trabalho)
